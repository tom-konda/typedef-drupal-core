/// <reference types="../drupal_settings" />

export type detachTrigger = 'unload'|'move'|'serialize';

type behaviorAttach = (context: Document|HTMLElement, settings?: Record<string, unknown>) => void
type behaviorDetach = (context: Document|HTMLElement, settings?: Record<string, unknown>, trigger?: detachTrigger) => void

type behavior = {
  attach: behaviorAttach,
  detach?: behaviorDetach,
} & Record<string, unknown>

type defaultBehavior = Record<string, behavior>

type replacementKeyPrefix = '!'|'@'|'%';
type replacementKey = `${replacementKeyPrefix}${string}`;

declare global {
  namespace drupalSettings {
    const suppressDeprecationErrors: boolean
  }
  namespace Drupal {
    interface definedBehaviorWithCustomProps {}
    export let locale: {}
    export function throwError(error: string|Error): void
    export const behaviors: defaultBehavior & {
      [key in keyof Drupal.definedBehaviorWithCustomProps]: Drupal.definedBehaviorWithCustomProps[key] & behavior;
    }
    export function attachBehaviors(context?: Document|HTMLElement, settings?: Record<string, unknown>): void
    export function detachBehaviors(context?: Document|HTMLElement, settings?: Record<string, unknown>, trigger?: detachTrigger): void
    export function checkPlain(str: string): string
    export function formatString(str: string, args?: Record<replacementKey, string>): string
    export function stringReplace(str: string, args: Record<string, string>, keys: null|Array<string>): string
    export function t(str: string, args?: Record<replacementKey, string>, options?: {langcode?: string, context?: string}): string
    export let url: {
      (path: string) : string,
      toAbsolute: (url: string) => string,
      isLocal: (url: string) => boolean,
    }
    export function formatPlural(count: number, singular: string, plural: string, args?: Record<replacementKey, string>, options?: {langcode?: string, context?: string}): string
    export function encodePath(item: string): string
    export function deprecationError(deprecation: {message: string}): void
    export function deprecatedProperty(deprecation: {target: Record<string, unknown>, deprecatedProperty: string, message: string}): Record<string, unknown>

    export function theme<T extends themeName>(func: T, ...args: themeArguments<T>): ReturnType<themeFunction<T>>
    namespace theme {
      let placeholder: (str: string) => string
    }

    type themeName = keyof typeof Drupal.theme

    type themeFunction<T extends themeName> = Pick<typeof Drupal.theme, themeName>[T]

    type themeArguments<T extends themeName> = Parameters<themeFunction<T>>

    export function elementIsVisible(elem: HTMLElement): boolean
    export function elementIsHidden(elem: HTMLElement): boolean
  }

  const drupalTranslations: undefined|{
    pluralFormula: {
      default: number,
    } & {[key: number]: number}
    strings: {
      [context: string]: {
        [original: string]: string,
      }
    }
  }
}
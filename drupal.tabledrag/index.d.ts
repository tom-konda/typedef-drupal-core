/// <reference types="../drupal" />

type rowSettings = {
  target: null|string,
  source: null|string,
  relationship: 'parent'|'sibling'|'self'|'group',
  action: 'match'|'order'|'depth',
  hidden: boolean,
  limit: number,
}

export type tableSettings = {
  [group: string]: {
    [tabledrag_id: number]: rowSettings
  }
}

type tableDrag = {
  $table: JQuery<HTMLTableElement>,
  table: HTMLTableElement & {topY ?: number, bottomY?: number},
  tableSettings: tableSettings,
  dragObject: null|HTMLElement,
  rowObject: null|tableDragRow,
  oldRowElement: null|HTMLElement,
  oldY: null|number,
  changed: boolean,
  maxDepth: number,
  rtl: -1|1,
  striping: boolean,
  scrollSettings: {amount: number, interval: number, trigger: number },
  scrollInterval: null|number,
  scrollY: number,
  windowHeight: number,
  $toggleWeightButton: null|JQuery<HTMLElement>,
  indentEnabled: boolean,
  changedRowIds: Set<string>,
  indentCount?: number,
  indentAmount?: number,
  safeBlur?: boolean,
  currentPointerCoords?: {
    x: number;
    y: number;
  },
  initColumns: () => void,
  addColspanClass: (columnIndex: number) => () => void,
  displayColumns: (displayWeight: boolean) => void,
  toggleColumns: () => void,
  hideColumns: () => void,
  showColumns: () => void,
  rowSettings: (group: string, row: HTMLElement) => rowSettings,
  makeDraggable: (item: HTMLElement) => void,
  dragStart: (event: JQuery.Event, self: tableDrag, item: HTMLElement) => void,
  dragRow: (event: JQuery.Event, self: tableDrag) => false|void,
  dropRow: (event: JQuery.Event, self: tableDrag) => void,
  pointerCoords: (event: JQuery.Event) => {x: number, y: number},
  getPointerOffset: (target: HTMLElement, event: JQuery.Event) => {x: number, y: number},
  findDropTargetRow: (x: number, y: number) => null|HTMLElement,
  updateFields: (changedRow: HTMLElement) => void,
  updateField: (changedRow: HTMLElement, group: string) => void,
  copyDragClasses: (sourceRow: HTMLElement, targetRow: HTMLElement, group: string) => void,
  checkScroll: (cursorY: number) => number,
  setScroll: (scrollAmount: number) => void,
  restripeTable: () => void,
  onDrag: () => unknown,
  onDrop: () => unknown,
  row: {
    new (tableRow: HTMLElement, method: tableDragRow['method'], indentEnabled: boolean, maxDepth: number, addClasses: boolean): tableDragRow,
    prototype: {
      findChildren: (this: tableDragRow, addClasses: boolean) => unknown[],
      isValidSwap: (this: tableDragRow, row: HTMLElement) => boolean,
      swap: (this: tableDragRow, position: string, row: HTMLElement) => void,
      validIndentInterval: (this: tableDragRow, prevRow: HTMLElement|null, nextRow: HTMLElement|null) => { min: number, max: number },
      indent: (this: tableDragRow, indentDiff: number) => number,
      findSiblings: (this: tableDragRow, rowSettings: rowSettings) => Array<JQuery<HTMLElement>>,
      addChangedWarning: (this: tableDragRow) => void,
      removeIndentClasses: (this: tableDragRow) => void,
      markChanged: (this: tableDragRow) => void,
      onIndent: (this: tableDragRow) => unknown,
      onSwap: (this: tableDragRow, swappedRow: HTMLElement) => unknown,
    }
  },
}

type tableDragRow = {
  element: HTMLElement,
  method: 'keyboard'|'mouse'|'pointer',
  group: Array<HTMLElement>,
  groupDepth: number,
  changed: boolean,
  table: HTMLTableElement,
  indentEnabled: boolean,
  maxDepth: number,
  direction: 'down'|'up',
  indents?: number,
  children?: unknown[],
  interval?: {
    min: number;
    max: number;
  }|null,
  findChildren: (addClasses: boolean) => unknown[],
  isValidSwap: (row: HTMLElement) => boolean,
  swap: (position: string, row: HTMLElement) => void,
  validIndentInterval: (prevRow: HTMLElement|null, nextRow: HTMLElement|null) => { min: number, max: number },
  indent: (indentDiff: number) => number,
  findSiblings: (rowSettings: rowSettings) => Array<HTMLElement>,
  removeIndentClasses: () => void,
  markChanged: () => void,
  onIndent: () => unknown,
  onSwap: (swappedRow: HTMLElement) => unknown,
}

declare global {
  namespace drupalSettings {
    const tableDrag: {
      [table_id: string]: tableSettings,
    }
  }
  namespace Drupal {
    export const tableDrag: {
      new (table: HTMLElement, tableSettings: tableSettings): tableDrag,
      prototype: {
        initColumns: (this: tableDrag) => void,
        addColspanClass: (this: tableDrag, columnIndex: number) => () => void,
        displayColumns: (this: tableDrag, displayWeight: boolean) => void,
        toggleColumns: (this: tableDrag) => void,
        hideColumns: (this: tableDrag) => void,
        showColumns: (this: tableDrag) => void,
        rowSettings: (this: tableDrag, group: string, row: HTMLElement) => rowSettings,
        makeDraggable: (this: tableDrag, item: HTMLElement) => void,
        dragStart: (this: tableDrag, event: JQuery.Event, self: tableDrag, item: HTMLElement) => void,
        dragRow: (this: tableDrag, event: JQuery.Event, self: tableDrag) => false|void,
        dropRow: (this: tableDrag, event: JQuery.Event, self: tableDrag) => void,
        pointerCoords: (this: tableDrag, event: JQuery.Event) => {x: number, y: number},
        getPointerOffset: (this: tableDrag, target: HTMLElement, event: JQuery.Event) => {x: number, y: number},
        findDropTargetRow: (this: tableDrag, x: number, y: number) => null|HTMLElement,
        updateFields: (this: tableDrag, changedRow: HTMLElement) => void,
        updateField: (this: tableDrag, changedRow: HTMLElement, group: string) => void,
        copyDragClasses: (this: tableDrag, sourceRow: HTMLElement, targetRow: HTMLElement, group: string) => void,
        checkScroll: (this: tableDrag, cursorY: number) => number,
        setScroll: (this: tableDrag, scrollAmount: number) => void,
        restripeTable: (this: tableDrag) => void,
        onDrag: (this: tableDrag) => unknown,
        onDrop: (this: tableDrag) => unknown,
        row: {
          new (tableRow: HTMLElement, method: 'keyboard'|'mouse'|'pointer', indentEnabled: boolean, maxDepth: number, addClasses: boolean): tableDragRow,
          prototype: {
            findChildren: (this: tableDragRow, addClasses: boolean) => unknown[],
            isValidSwap: (this: tableDragRow, row: HTMLElement) => boolean,
            swap: (this: tableDragRow, position: string, row: HTMLElement) => void,
            validIndentInterval: (this: tableDragRow, prevRow: HTMLElement|null, nextRow: HTMLElement|null) => { min: number, max: number },
            indent: (this: tableDragRow, indentDiff: number) => number,
            findSiblings: (this: tableDragRow, rowSettings: rowSettings) => Array<HTMLElement>,
            addChangedWarning: (this: tableDragRow) => void,
            removeIndentClasses: (this: tableDragRow) => void,
            markChanged: (this: tableDragRow) => void,
            onIndent: (this: tableDragRow) => unknown,
            onSwap: (this: tableDragRow, swappedRow: HTMLElement) => unknown,
          }
        }
      }
    } & Record<string, tableDrag>
    namespace theme {
      let tableDragChangedMarker: () => string
      let tableDragIndentation: () => string
      let tableDragChangedWarning: () => string
      let tableDragToggle: () => string
      let toggleButtonContent: (show: boolean) => string
      let tableDragHandle: () => string
    }
  }
}

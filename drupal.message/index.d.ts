/// <reference types="../drupal" />
/// <reference types="../drupal.announce" />

import type { ariaLiveValue } from "../drupal.announce";

declare global {
  namespace Drupal {
    class Message {
      constructor (messageWrapper?: HTMLElement)
      static defaultWrapper: () => Element
      static getMessageTypeLabels: () => {
        status: string,
        error: string,
        warning: string,
      }
      static announce: (message: string, options?: {announce: string, priority?: ariaLiveValue, type: messageType}) => void
      static messageInternalWrapper: (messageWrapper: HTMLElement) => HTMLElement
      add: (message: string, options?: messageOptions) => string
      select: (id: string) => HTMLElement
      remove: (id: string) => number
      clear: () => void
    }

    namespace theme {
      let message: (message: {text: string}, options: {type: messageType, id: string}) => HTMLElement
    }
  }
}

export type messageType = 'status'|'error'|'warning';

export type messageOptions = {
  id?: string,
  type?: messageType,
  announce?: string,
  priority?: string,
}
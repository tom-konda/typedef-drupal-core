/// <reference types="../drupal" />
/// <reference types="../drupal.debounce" />

declare global {
  export namespace Drupal {
    export const displace: {
      (broadcast?: boolean): displaceOffset,
      readonly offsets: displaceOffset,
      calculateOffset: <E extends keyof displaceOffset>(edge: E) => displaceOffset[E],
    }
  }
}

export type displaceOffset = {
  top: number,
  right: number,
  bottom: number,
  left: number,
}

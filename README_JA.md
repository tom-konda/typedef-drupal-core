# TypeScript type definitions for Drupal core

## インストール方法

1. `cd /path/to/DRUPAL_ROOT`
2. `yarn add https://gitlab.com/tom-konda/typedef-drupal-core.git` または `npm install https://gitlab.com/tom-konda/typedef-drupal-core.git`

## 使用方法

[使用例のレポジトリ](https://gitlab.com/tom-konda/typedef-drupal-core-examples)を確認してください

## Drupal 特有の型の拡張方法

### drupalSettings

```ts:custom.d.ts
declare global {
  namespace drupalSettings {
    const someVariable: string|number
  }
}

/*
 * If there is no export/import in a d.ts file, TS2669 error occurs by TypeScript.
 * See https://stackoverflow.com/questions/57132428/augmentations-for-the-global-scope-can-only-be-directly-nested-in-external-modul.
 */
export type {}
```

### Drupal.theme

```ts:custom.d.ts
declare global {
  namespace Drupal {
    namespace theme {
      let someTheme: (arg1: string) => HTMLElement
    }
  }
}

export type {}
```

### カスタムプロパティ付きの Behavior

```ts:custom.d.ts
declare global {
  namespace Drupal {
    interface definedBehaviorWithCustomProps {
      someBehavior : {
        someProp: Array<string>
      }
    }
  }
}

export type {}
```

### AjaxCommand

```ts:custom.d.ts
import type { ajaxCommand, commandDefinition } from "drupal.ajax"

declare global {
  namespace Drupal {
    namespace Ajax {
      interface definedCommands {
        someCommand: ajaxCommand<
          'someCommand',
          Pick<commandDefinition, 'selector'> & {
            someSettings: number,
          },
          boolean
        >
      }
    }
  }
}
```

## ソフトウェアへの貢献方法

貢献は歓迎です。\
issue か MR の作成をお願いします。


## ライセンス

このソフトウェアは GNU GPL v2.0 or later ライセンスとなります。
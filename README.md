# TypeScript type definitions for Drupal core

## Installation

1. `cd /path/to/DRUPAL_ROOT`
2. `yarn add https://gitlab.com/tom-konda/typedef-drupal-core.git` or `npm install https://gitlab.com/tom-konda/typedef-drupal-core.git`

## Usage

See [example repository](https://gitlab.com/tom-konda/typedef-drupal-core-examples).

## Extend Drupal specific type

### drupalSettings

```ts:custom.d.ts
declare global {
  namespace drupalSettings {
    const someVariable: string|number
  }
}

/*
 * If there is no export/import in a d.ts file, TS2669 error occurs by TypeScript.
 * See https://stackoverflow.com/questions/57132428/augmentations-for-the-global-scope-can-only-be-directly-nested-in-external-modul.
 */
export type {}
```

### Drupal.theme

```ts:custom.d.ts
declare global {
  namespace Drupal {
    namespace theme {
      let someTheme: (arg1: string) => HTMLElement
    }
  }
}

export type {}
```

### A behavior with some properties

```ts:custom.d.ts
declare global {
  namespace Drupal {
    interface definedBehaviorWithCustomProps {
      someBehavior : {
        someProp: Array<string>
      }
    }
  }
}

export type {}
```

### AjaxCommand

```ts:custom.d.ts
import type { ajaxCommand, commandDefinition } from "drupal.ajax"

declare global {
  namespace Drupal {
    namespace Ajax {
      interface definedCommands {
        someCommand: ajaxCommand<
          'someCommand',
          Pick<commandDefinition, 'selector'> & {
            someSettings: number,
          },
          boolean
        >
      }
    }
  }
}
```

## Contributing

Contributions are welcomed.\
Please create an issue or a merge request.

## License

This software is licensed under the GNU GPL v2.0 or later.
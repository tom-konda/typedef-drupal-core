/// <reference types="../drupal" />
/// <reference types="../drupal.displace" />

import type { displaceOffset } from "../drupal.displace";

type tableHeader = {
  $originalTable: JQuery<HTMLElement>,
  $originalHeader: JQuery<HTMLElement>,
  $originalHeaderCells: Array<JQuery<HTMLElement>>,
  displayWeight: null|number,
  tableHeight: null|number,
  tableOffset: null|displaceOffset,
  tables: Array<tableHeader>,
  minHeight: number,
  stickyVisible: boolean,
  $html?: JQuery<HTMLElement>,
  $stickyTable?: JQuery<HTMLElement>,
  $stickyHeaderCells?: Array<JQuery<HTMLElement>>,
  createSticky: () => void,
  stickyPosition: (offsetTop: number, offsetLeft: number) => JQuery<HTMLElement>,
  checkStickyVisible: () => boolean,
  onScroll: (e: JQuery.Event) => void,
  recalculateSticky: (event?: JQuery.Event) => void,
}
declare global {
  namespace Drupal {

    export const TableHeader: {
      new (table: HTMLElement): tableHeader,
      prototype: {
        createSticky: (this: tableHeader) => void,
        stickyPosition: (this: tableHeader, offsetTop: number, offsetLeft: number) => JQuery<HTMLTableElement>,
        checkStickyVisible: (this: tableHeader) => boolean,
        onScroll: (this: tableHeader, e: JQuery.Event) => void,
        recalculateSticky: (this: tableHeader, event: JQuery.Event) => void,
      }
    }
  }
}

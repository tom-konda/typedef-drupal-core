/// <reference types="../drupal" />
/// <reference types="../drupal.debounce" />

declare global {
  namespace Drupal {
    export function announce(text: string, priority?: ariaLiveValue): void
  }
}

export type ariaLiveValue = 'off'|'polite'|'assertive'
/// <reference types="../drupal" />

export type ProgressBar = {
  id: string,
  method: string,
  updateCallback: (percentage: number, message: string, progressBar: ProgressBar) => void,
  errorCallback: (progressBar: ProgressBar) => void,
  element: JQuery<HTMLElement>,
  delay: number,
  uri: string|null,
  timer: number,
  setProgress: (percentage: number, message: string, label?: string) => void,
  startMonitoring: (uri: string, delay: number) => void,
  stopMonitoring: () => void,
  sendPing: () => void,
  displayError: (string: string) => void,
}

declare global {
  namespace Drupal {

    const ProgressBar: {
      new (id: string, updateCallback?: unknown, method?: string, errorCallback?: unknown): ProgressBar,
      prototype: {
        setProgress: (this: ProgressBar, percentage: number, message: string, label: string) => void,
        startMonitoring: (this: ProgressBar, uri: string, delay: number) => void,
        stopMonitoring: (this: ProgressBar) => void,
        sendPing: (this: ProgressBar) => void,
        displayError: (this: ProgressBar, string: string) => void,
      },
    }
    namespace theme {
      let progressBar: (id: string) => string
    }
  }
}

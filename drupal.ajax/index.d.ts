/// <reference types="../drupal" />
/// <reference types="../drupal.message" />
/// <reference types="../drupal.progress" />

import type { ariaLiveValue } from "drupal.announce";
import type { messageOptions } from "../drupal.message";
import type { ProgressBar } from "../drupal.progress";

interface AjaxError extends Error {
}

interface AjaxErrorConstructor extends ErrorConstructor {
    new(xmlhttp: XMLHttpRequest|JQuery.jqXHR<any>, uri: string, customMessage?: string): AjaxError;
    (xmlhttp: XMLHttpRequest|JQuery.jqXHR<any>, uri: string, customMessage?: string): AjaxError;
    readonly prototype: AjaxError;
}

/** @see https://api.drupal.org/api/drupal/core!core.api.php/group/ajax */
export type commandDefinition = {
  command: string,
  method: 'replaceWith'|'append'|'prepend'|'before'|'after'|'html',
  selector: string,
  data: string|Array<Record<string, string>>,
  settings: Record<string, unknown>,
  asterisk: string,
  text: string,
  title: string,
  url: string,
  argument: Record<string, string>,
  name: string,
  value: string|Record<string, unknown>,
  old: string,
  new: string,
  merge: boolean,
  args: Array<unknown>,
}

export type ajaxCommand<K extends string, P, R = never> = (ajax: Ajax, response: {command: K} & P, status: number) => void|R;

type progressType = 'throbber'|'bar'|'fullscreen';

type speedValueType = 'none'|'slow'|'fast'|number;

export type Ajax = {
  commands: Drupal.Ajax.definedCommands & Record<string, (ajax: Ajax, response: {'command': string} & Record<string, unknown>, status: number) => unknown>,
  instanceIndex: false|number,
  element: HTMLElement|false,
  preCommandsFocusedElementSelector: string|null,
  elementSettings: Drupal.Ajax.elementSettings,
  $form?: JQuery<HTMLElement>,
  url: string,
  options: JQueryAjaxSettings & {
    isInProgress(): boolean,
  },
  ajaxing: boolean,
  settings: unknown,
  execute: () => void|JQuery.jqXHR<any>|JQuery.Deferred<any, any, any>,
  keypressResponse: (element: HTMLElement, event: JQuery.Event) => void,
  eventResponse: (element: HTMLElement, event: JQuery.Event) => void,
  beforeSerialize: (element: HTMLElement, options: JQueryAjaxSettings) => void,
  beforeSubmit: (formValues: Array<Record<string, unknown>>, element: JQuery<HTMLElement>, options: JQueryAjaxSettings) => void,
  beforeSend: (xmlhttprequest: XMLHttpRequest, options: JQueryAjaxSettings) => void,
  commandExecutionQueue: (response: Array<commandDefinition>, status: number|JQuery.Ajax.SuccessTextStatus) => Promise<void>,
  success: (response: Array<commandDefinition>, status: number|JQuery.Ajax.SuccessTextStatus) => Promise<void>,
  getEffect: (response: commandDefinition) => {
    showEffect: 'show'|'fadeIn'|'slideToggle',
    hideEffect: 'hide'|'fadeOut'|'slideToggle',
    showSpeed: speedValueType,
  },
  error: (xmlhttprequest: XMLHttpRequest, uri: string, customMessage?: string) => void,
  httpMethod: 'POST'|'GET'
} & {
  [key in progressType as `setProgressIndicator${Capitalize<progressType>}`]: () => void
} & Drupal.Ajax.elementSettings

declare global {
  namespace drupalSettings {
    const ajaxPageState: {
      libraries: string,
      theme: null|string,
      theme_token: string,
    }
    const ajaxTrustedUrl: Record<string, true>
    const ajax: {[base: string]: Drupal.Ajax.elementSettings}
  }
  namespace Drupal {
    export const AjaxError: AjaxErrorConstructor
    export const ajax: {
      (settings: Drupal.Ajax.elementSettings & {base?: string, element?: HTMLElement}): Ajax,
      instances: Array<Ajax>,
      expired: () => Array<Ajax>,
      bindAjaxLinks: (element: HTMLElement) => void,
      WRAPPER_FORMAT: string,
    }
    export const Ajax: {
      new (base: string|false, element: HTMLElement|false, settings: Exclude<Drupal.Ajax.elementSettings, 'base'|'element'>): Ajax,
      AJAX_REQUEST_PARAMETER: string,
      prototype: {
        execute: (this: Ajax) => void|JQuery.jqXHR<any>|JQuery.Deferred<any, any, any>,
        keypressResponse: (this: Ajax, element: HTMLElement, event: JQuery.Event) => void,
        eventResponse: (this: Ajax, element: HTMLElement, event: JQuery.Event) => void,
        beforeSerialize: (this: Ajax, element: HTMLElement, options: JQueryAjaxSettings) => void,
        beforeSubmit: (this: Ajax, formValues: Array<Record<string, unknown>>, element: JQuery<HTMLElement>, options: JQueryAjaxSettings) => void,
        beforeSend: (this: Ajax, xmlhttprequest: XMLHttpRequest, options: JQueryAjaxSettings) => void,
        commandExecutionQueue: (this: Ajax, response: Array<commandDefinition>, status: number|JQuery.Ajax.SuccessTextStatus) => Promise<void>,
        success: (this: Ajax, response: Array<commandDefinition>, status: number|JQuery.Ajax.SuccessTextStatus) => Promise<void>,
        getEffect: (this: Ajax, response: commandDefinition) => {showEffect: string, hideEffect: string, showSpeed: string}|{},
        error: (this: Ajax, xmlhttprequest: XMLHttpRequest, uri: string, customMessage?: string) => void,
      } & {
        [key in progressType as `setProgressIndicator${Capitalize<progressType>}`]: (this: Ajax) => void;
      },
    }
    namespace Ajax {
      interface elementSettings {
        wrapper?: string,
        method?: 'replaceWith'|'append'|'prepend'|'before'|'after'|'html'|'replaceAll'|'empty'|'remove'|'focus',
        effect?: 'none'|'slide'|'fade',
        speed?: speedValueType,
        event?: string|null,
        prevent?: string,
        progress?: {
          type: progressType,
          message?: string,
          url?: string,
          interval?: number,
          element?: HTMLElement|JQuery<HTMLElement>,
          object?: ProgressBar,
          method?: string,
        }|false,
        url?: string,
        httpMethod?: 'POST'|'GET',
        keypress?: boolean,
        selector?: string|null,
        submit?: {
          js: boolean,
        },
        /** @see https://www.drupal.org/docs/drupal-apis/ajax-api/ajax-dialog-boxes */
        dialog?: JQueryUI.DialogOptions,
        dialogType?: 'modal'|'dialog',
        dialogRenderer?: 'off_canvas'|'off_canvas_top'|string,

        setClick?: boolean,
      }
      interface definedCommands {
        /** @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Ajax%21InsertCommand.php/class/InsertCommand */
        insert: ajaxCommand<
          'insert',
          Pick<commandDefinition, 'data'> &
          Partial<Pick<commandDefinition, 'method'|'selector'|'settings'>>
        >
        /** @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Ajax%21RemoveCommand.php/class/RemoveCommand */
        remove: ajaxCommand<
          'remove',
          Pick<commandDefinition, 'selector'> &
          Partial<Pick<commandDefinition, 'settings'>>
        >
        /** @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Ajax%21ChangedCommand.php/class/ChangedCommand */
        changed: ajaxCommand<
          'changed',
          Pick<commandDefinition, 'selector'> &
          Partial<Pick<commandDefinition, 'asterisk'>>
        >
        /** @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Ajax%21AlertCommand.php/class/AlertCommand */
        alert: ajaxCommand<'alert', Pick<commandDefinition, 'text'>>
        /** @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Ajax%21AnnounceCommand.php/class/AnnounceCommand */
        announce: ajaxCommand<'announce', Pick<commandDefinition, 'text'> & {priority?: ariaLiveValue}>
        /** @see https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Ajax!RedirectCommand.php/class/RedirectCommand */
        redirect: ajaxCommand<'redirect', Pick<commandDefinition, 'url'>>
        /** @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Ajax%21CssCommand.php/class/CssCommand */
        css: ajaxCommand<'css', Pick<commandDefinition, 'selector'|'argument'>>
        /** @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Ajax%21SettingsCommand.php/class/SettingsCommand */
        settings: ajaxCommand<'settings', Pick<commandDefinition, 'merge'|'settings'>>
        /** @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Ajax%21DataCommand.php/class/DataCommand */
        data: ajaxCommand<'data', Pick<commandDefinition, 'name'|'selector'|'value'>>
        /** @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Ajax%21FocusFirstCommand.php/class/FocusFirstCommand */
        focusFirst: ajaxCommand<'focusFirst', Pick<commandDefinition, 'selector'>>
        /** @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Ajax%21InvokeCommand.php/class/InvokeCommand */
        invoke: ajaxCommand<'invoke', Pick<commandDefinition, 'args'|'method'|'selector'>>
        /** @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Ajax%21RestripeCommand.php/class/RestripeCommand */
        restripe: ajaxCommand<'restripe', Pick<commandDefinition, 'selector'>>
        /** @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Ajax%21UpdateBuildIdCommand.php/class/UpdateBuildIdCommand */
        update_build_id: ajaxCommand<'update_build_id', Pick<commandDefinition, 'old'|'new'>>
        /** @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Ajax%21AddCssCommand.php/class/AddCssCommand */
        add_css: ajaxCommand<'add_css', Pick<commandDefinition, 'data'>>
        /** @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Ajax%21MessageCommand.php/class/MessageCommand */
        message: ajaxCommand<
          'message',
          {
            message: string,
            messageWrapperQuerySelector: string|null,
            messageOptions?: messageOptions,
            clearPrevious: boolean
          }
        >
        /** @see https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Ajax!AddJsCommand.php/class/AddJsCommand */
        add_js: ajaxCommand<
          'add_js',
          Pick<commandDefinition, 'selector'> & {
            data: Array<{
              src: string
            } & Record<string, string>>
          },
          Promise<void>
        >
        /** @see https://api.drupal.org/api/drupal/core!lib!Drupal!Core!Ajax!ScrollTopCommand.php/class/ScrollTopCommand */
        scrollTop: ajaxCommand<
          'scrollTop',
          Pick<commandDefinition, 'selector'>
        >
      }
    }

    export const AjaxCommands: {
      new(): Drupal.Ajax.definedCommands & Record<string, (ajax: Ajax, response: {'command': string} & Record<string, unknown>, status: number) => unknown>,
      prototype: Drupal.Ajax.definedCommands & Record<string, (ajax: Ajax, response: {'command': string} & Record<string, unknown>, status: number) => unknown>,
    }
    namespace theme {
      /** @deprecated see https://www.drupal.org/node/2940704 */
      let ajaxWrapperNewContent: ($newContent: JQuery<HTMLElement>, ajax: Ajax, response: commandDefinition) => JQuery<HTMLElement>
      /** @deprecated see https://www.drupal.org/node/2940704 */
      let ajaxWrapperMultipleRootElements: ($elements: JQuery<HTMLElement>) => JQuery<HTMLElement>
      let ajaxProgressBar: ($element: JQuery<HTMLElement>) => JQuery<HTMLElement>
      let ajaxProgressThrobber: (message: string) => string
      let ajaxProgressMessage: (message: string) => string
      let ajaxProgressIndicatorFullscreen: () => string
    }
  }
}

/// <reference types="../drupal" />

declare global {
  namespace Drupal {
    export function debounce(func: (event: unknown) => unknown, wait: number, immediate?: boolean): unknown
  }
}

export type {}
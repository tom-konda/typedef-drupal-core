/// <reference types="../drupal" />
/// <reference types="../@drupal__once" />

type DropButton = {
  $dropbutton: JQuery<HTMLElement>,
  $list: JQuery<HTMLElement>,
  $actions: JQuery<HTMLElement>,
  dropbuttons: DropButton[],
  timerID?: number,
  toggle: (show: boolean) => void,
  hoverIn: () => void,
  hoverOut: () => void,
  open: () => void,
  close: () => void,
  focusOut: (e: JQuery.FocusOutEvent) => void,
  focusIn: (e: JQuery.FocusInEvent) => void,
}

declare global {
  namespace Drupal {
    export const DropButton: {
      new (dropbutton: HTMLElement, settings: {title: string}): DropButton,
      prototype: {
        toggle: (this: DropButton, show: boolean) => void,
        hoverIn: (this: DropButton) => void,
        hoverOut: (this: DropButton) => void,
        open: (this: DropButton) => void,
        close: (this: DropButton) => void,
        focusOut: (this: DropButton, e: JQuery.FocusOutEvent) => void,
        focusIn: (this: DropButton, e: JQuery.FocusInEvent) => void,
      }
    }
    namespace theme {
      let dropbuttonToggle: (options: {title: string}) => string
    }
  }
}

export type {}
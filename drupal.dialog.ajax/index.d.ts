///<reference types="../drupal.dialog" />
///<reference types="../drupal.ajax" />

import type { ajaxCommand, commandDefinition } from "../drupal.ajax"

declare global {
  namespace Drupal {
    interface definedBehaviorWithCustomProps {
      dialog: {
        prepareDialogButtons($dialog: JQuery<HTMLElement>): Array<JQueryUI.DialogButtonOptions>
      }
    }
    namespace Ajax {
      interface definedCommands {
        /** @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Ajax%21OpenDialogCommand.php/class/OpenDialogCommand */
        openDialog: ajaxCommand<
          'openDialog',
          Pick<commandDefinition, 'selector'|'data'|'settings'> & {
            dialogOptions: JQueryUI.DialogOptions & {drupalAutoButtons?: boolean, drupalOffCanvasPosition?: 'side'|'top'},
            effect?: 'fade',
            speed?: number,
          },
          false
        >
        /** @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Ajax%21CloseDialogCommand.php/class/CloseDialogCommand */
        closeDialog: ajaxCommand<
          'closeDialog',
          Pick<commandDefinition, 'selector'> & {
            persist: boolean,
          }
        >,
        /** @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Ajax%21SetDialogOptionCommand.php/class/SetDialogOptionCommand */
        setDialogOption: ajaxCommand<
          'setDialogOption',
          Pick<commandDefinition, 'selector'> & {
            optionName: string,
            optionValue: unknown,
          }
        >,
      } 
    }
  }
}

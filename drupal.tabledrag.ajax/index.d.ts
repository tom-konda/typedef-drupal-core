import type { ajaxCommand } from "../drupal.ajax"

declare global {
  namespace Drupal {
    namespace Ajax {
      interface definedCommands {
        tabledragChanged: ajaxCommand<
          'tabledragChanged',
          {
            id: string,
            tabledrag_instance: string,
          }
        >
      } 
    }
  }
}

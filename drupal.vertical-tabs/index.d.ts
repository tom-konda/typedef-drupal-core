export type verticalTabSettings = {
  title: string,
  details: JQuery<HTMLElement>,
}

type verticalTab = {
  details: JQuery<HTMLElement>,
  item: JQuery<HTMLElement>,
  link: JQuery<HTMLElement>,
  title: JQuery<HTMLElement>,
  summary: JQuery<HTMLElement>,
  focus: () => void,
  updateSummary: () => void,
  tabShow: () => verticalTab,
  tabHide: () => verticalTab,
}

declare global {
  namespace drupalSettings {
    const widthBreakpoint: number|undefined
  }
  namespace Drupal {

    export const verticalTab: {
      new (settings: verticalTabSettings): verticalTab,
      prototype: {
        focus: (this: verticalTab) => void,
        updateSummary: (this: verticalTab) => void,
        tabShow: (this: verticalTab) => verticalTab,
        tabHide: (this: verticalTab) => verticalTab,
      }
    }
    namespace theme {
      let verticalTab: (settings: {title: string}) => {
        item: JQuery<HTMLElement>,
        link: JQuery<HTMLElement>,
        title: JQuery<HTMLElement>,
        summary: JQuery<HTMLElement>,
      }
    }
  }
}
